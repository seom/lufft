'use strict';

    const net = require('net');
    const config = require('./config');
    const logger =require('./logger');
    const nodemailer = require('nodemailer');

    logger.info('--------------------------------');
    logger.info('LUFFT Ventus UMB reading script.');
    logger.info('--------------------------------');

    // ----------------------------------------------------
    var retrying = false;

    // Create socket and bind callbacks
    var socket = new net.Socket();
    socket.on('connect', connectEventHandler);
    socket.on('data',    dataEventHandler);
    socket.on('end',     endEventHandler);
    socket.on('timeout', timeoutEventHandler);
    socket.on('drain',   drainEventHandler);
    socket.on('error',   errorEventHandler);
    socket.on('close',   closeEventHandler);

    makeConnection();

    const weather = require("./weather")(socket);

    setInterval(() => { weather.timer_1_sec() }, 1000);

    //if (config.mail.mailUse){
    //    setInterval(() => { mail_timer() }, 5*60*1000); //5 min
    //}

    // ----------------------------------------------------

    // Functions to handle socket events
    function makeConnection () {
        // Connect
        logger.info('Connecting to meteo host ' + config.meteo.host + ':' + config.meteo.port + '...');
        socket.connect(config.meteo.port, config.meteo.host);
    }

    function connectEventHandler() {
        logger.info('Socket connected!');
        retrying = false;

        if (!weather.getTemp()) {
            logger.error('Can\'t get Temp after connect ');

            setTimeout(function() {
                weather.getTemp();
            }, 1000);

        }
    }

    function dataEventHandler(data) {
        logger.debug(data.toString());

        var strMeasurementValue = data.toString().substr(10, 6);
        //logger.debug(strMeasurementValue);

        if (strMeasurementValue.indexOf('00100') == 0) {
            weather.setTemp(data);

            setTimeout(function() {
                weather.getPressure();
            }, config.meteo.requestDelay);

        } else if (strMeasurementValue.indexOf('00300') == 0) {
            weather.setPressure(data);

            setTimeout(function() {
              weather.getWind();
            }, config.meteo.requestDelay);

        } else if (strMeasurementValue.indexOf('00400') == 0) {
            weather.setWind(data);

            setTimeout(function() {
              weather.getWindDir();
            }, config.meteo.requestDelay);

        } else if (strMeasurementValue.indexOf('00500') == 0) {
            weather.setWindDir(data);

            weather.printCurWeather();

          setTimeout(function() {
              weather.getTemp();
          }, config.meteo.requestDelay);

        }
        else {
            logger.warn('Не прийшло даних з сокету!');
        //    setTimeout(function() {
        //        weather.getTemp();
        //    }, 1000);
        }
    }

    function endEventHandler() {
        logger.info('Socket end!');
    }
    function timeoutEventHandler() {
        logger.info('Socket timeout!');
    }
    function drainEventHandler() {
        logger.info('Socket drain!');
    }
    function errorEventHandler() {
        logger.info('Socket error!');
    }

    function closeEventHandler () {
        logger.info('Socket close!');
        if (!retrying) {
            retrying = true;
            logger.info('Reconnecting...');
        }
        setTimeout(makeConnection, config.meteo.reconnectTimeout);
    }

    // ----------------------------------------------------
    // HTTP Server
    if(config.httpServer.httpUse){

        var http = require('http');

        var responseBody = {};

        let srv = http.createServer(function(request, response) {
            request.on('error', function(err) {
                logger.error(err);
                response.statusCode = 400;
                response.end();
            });
            response.on('error', function(err) {
                logger.error(err);
            });
            if (request.method === 'GET' && request.url === '/') {
                response.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'});
                response.write('<b>Метеостанція '+config.station+'<\/b><br>');
                response.write('<p><a href="http://'+request.headers.host+'/meteo">'+'Метеодані'+'<\/a>');
                response.write(' | ');
                response.write('<a href="http://'+request.headers.host+'/config">'+'Конфігурація'+'<\/a>');
                response.end();
            }
            else if (request.method === 'GET' && request.url === '/meteo') {
                response.writeHead(200, {'Content-Type': 'application/json'}); //'Content-Type': 'text/html;charset=utf-8'

                let data = weather.getMeteoData();
                responseBody = {
                    service: 'dts_meteo2db_lufft ' + '(Mode: ' + (process.env.NODE_ENV || 'development') + ')',
                    station: config.station + ' (' + config.meteo.host + ':' + config.meteo.port + ')',
                    temp: data.temperature,
                    pressure: data.pressure,
                    windSpeed: weather.arrWindSpeed.toString(),
                    windSpeedAvg: data.windSpeedAvg,
                    windSpeedMax: data.windSpeedMax,
                    windDir: weather.arrWindDir.toString(),
                    visibility: data.visibility,
                    lastDT: (weather.lastDT) ? new Date(weather.lastDT).toLocaleTimeString() : -1
                };

                //var responseBody = {
                //    service: 'dts_meteo2db_lufft ' + '(Mode: ' + (process.env.NODE_ENV || 'development') + ')',
                //    station: config.station + ' (' + config.meteo.HOST + ':' + config.meteo.PORT + ')',
                //    temp: weather.temp,
                //    pressure: weather.pressure,
                //    windSpeed: weather.windSpeed,
                //    windDir: weather.windDir,
                //    visibility: weather.visibility,
                //    lastDT: new Date(weather.lastDT).toLocaleTimeString()
                //};

                response.write(JSON.stringify(responseBody));
                //response.write('<a id=refresh href="">'+'Оновити'+'<\/a>');
                //response.write('<script>document.getElementById.addEventListener("click", function() {window.location.reload(false)});<\/script>');
                //document.getElementById.addEventListener( "click" , function() {window.location.reload(false)});
                response.end(); //response.end('<html><body><h1>Hello, World!</h1></body></html>');
            } else if (request.method === 'GET' && request.url === '/config') {
                response.writeHead(200, {'Content-Type': 'application/json'}); //'Content-Type': 'text/html;charset=utf-8'

                //responseBody = config;

                responseBody = {}; // новий пустий объект

                // клоную config (із урахуванням вкладених обєктів!)
                for (var key in config) {
                    if (!config.hasOwnProperty(key)) continue;
                    if(typeof(config[key]) == 'object'){
                        responseBody[key] = {};
                        for (var key1 in config[key]) {
                            if (!config[key].hasOwnProperty(key1)) continue;
                            responseBody[key][key1] = config[key][key1];
                        }
                    } else {
                        responseBody[key] = config[key];
                    }
                }
                // Шлях до БД Firebird обрізаю до вигляду BDLK_PLANSHET
                var arr = config && config.fb.database && config.fb.database.split('\\\\', 5);
                if (arr && arr[4]) responseBody.fb.database = arr[4].slice(0, arr[4].indexOf('.'));

                // Пишу у response та відсікаю усі поля password та role!
                response.write(JSON.stringify(responseBody, function(key, value) {
                    if (key == 'password' || key == 'role') return undefined; // паролі не відображаю! Можна так: delete responseBody.pg.password;
                    return value;
                }));

                response.end();
            } else {
                //response.statusCode = 404;
                response.writeHead(404, {'Content-Type': 'text/html;charset=utf-8'});
                response.end('Метеодані доступні за адресою: <a href="http://'+request.headers.host+'/meteo">http://'+request.headers.host+'/meteo<\/a>');
            }
            // Логую запроси до HTTP Серверу
            if (request.method === 'GET'){
                let ip = request.headers['x-forwarded-for'] ||
                        request.connection.remoteAddress ||
                        request.socket.remoteAddress ||
                        request.connection.socket.remoteAddress;
                logger.info('[http access]:' + ' [url: ' + request.url + '] [remoteAddress: ' + ip + ']');
            }
        });

        srv.listen(config.httpServer.httpPort);
    }
    // ----------------------------------------------------

    // ----------------------------------------------------
    //// Mail
    //
    //let smtpConfig = {
    //    host: 'mail.vts',
    //    port: 25,
    //    ignoreTLS: true,
    //    secure: false, // upgrade later with STARTTLS
    //    auth: {
    //        type: 'login',
    //        user: 'kulik',
    //        pass: 'e67HG5W'
    //        //user: 'pilotrk',
    //        //pass: 'd4G2kR9'
    //
    //    }
    //};
    //let transporter = nodemailer.createTransport(smtpConfig);
    ////
    ////
    ////// setup email data with unicode symbols
    //var mailOptions = {
    //    from: '"LUFFT Meteostation" kulik@vts.delta-pilot.ua', // sender address
    //    //to: 'seom.aa@gmail.com', // list of receivers
    //    //to: 'dmitrov@vts.delta-pilot.ua, seom.aa@gmail.com', // list of receivers
    //    cc: 'kulik@vts.delta-pilot.ua',
    //    to: 'seom.aa@gmail.com', // list of receivers
    //    subject: 'Problem: LUFFT Meteostation.', // Subject line
    //    text: 'Метеостанція LUFFT Ventus-UMB (' + config.meteo.host + ':' + config.meteo.port + ') недосяжна. Дані востаннє були отримані ' + weather.lastDT, // plain text body
    //    html: '<b>Не можу отримати дані з метеостанції LUFFT Ventus-UMB (' + config.meteo.host + ':' + config.meteo.port + '). Дані востаннє були отримані ' + weather.lastDT + '</b>' // html body
    //};
    //
    //// send mail with defined transport object
    ////setTimeout(() => {
    ////    var mailOptions = {
    ////        //from: '"LUFFT Meteostation"', // sender address
    ////        //to: 'seom.aa@gmail.com', // list of receivers
    ////        //to: 'dmitrov@vts.delta-pilot.ua, seom.aa@gmail.com', // list of receivers
    ////        to: 'kulik@vts.delta-pilot.ua', // list of receivers
    ////        subject: 'Problem: LUFFT Meteostation.', // Subject line
    ////        text: 'Метеостанція LUFFT Ventus-UMB (' + config.meteo.HOST + ':' + config.meteo.PORT + ') недосяжна. Дані востаннє були отримані ' + weather.lastDT, // plain text body
    ////        html: '<b>Не можу отримати дані з метеостанції LUFFT Ventus-UMB (' + config.meteo.HOST + ':' + config.meteo.PORT + '). Дані востаннє були отримані ' + weather.lastDT + '</b>' // html body
    ////    };
    ////    transporter.sendMail(mailOptions, (error, info) => {
    ////    if (error) {
    ////        return logger.error(error);
    ////    }
    ////    logger.info(weather.lastDT);
    ////    logger.info(mailOptions);
    ////    logger.info('Message %s sent: %s', info.messageId, info.response);
    ////}) }, 5000);
    //
    //transporter.sendMail(mailOptions, (error, info) => {
    //    if (error) {
    //        return logger.error(error);
    //    }
    //    logger.info('Message %s sent: %s', info.messageId, info.response);
    //});
    //// ----------------------------------------------------
    //
    //// create reusable transporter object using the default SMTP transport
    ////var transporter = nodemailer.createTransport({
    ////    service: 'gmail',
    ////    auth: {
    ////        user: 'seom.aa@gmail.com',
    ////        pass: 'seoms0rrow'
    ////    }
    ////});
    ////
    ////// verify connection configuration
    ////transporter.verify(function(error, success) {
    ////    if (error) {
    ////        logger.error(error);
    ////    } else {
    ////        logger.debug('Mail Server is ready to take our messages');
    ////    }
    ////});
    //
    //function mail_timer(){
    //    // Кожні 10 хв. (config.mail.mailTimeout) перевіряю, чи отримувались дані
    //    logger.debug('Mail timer.');
    //    const now = +Date.now();
    //    logger.debug('Last data:' + now - weather.lastDT);
    //    if ((now - weather.lastDT) >= config.mail.mailTimeout){
    //        // дані не отримувались протягом config.mail.mailTimeout
    //        var mailOptions = {
    //            from: '"LUFFT Meteostation script"', // sender address
    //            to: config.mail.mailTo, // list of receivers
    //            subject: 'Problem: LUFFT Meteostation.', // Subject line
    //            text: '[Пост: ' + config.post +', '+config.host+'] Не можу отримати дані з метеостанції LUFFT Ventus-UMB (' + config.meteo.host + ':' + config.meteo.port + '). Дані востаннє були отримані ' + new Date(weather.lastDT).toLocaleTimeString(), // plain text body
    //            html: '<b>[Пост: ' + config.post +', '+config.host+'] Не можу отримати дані з метеостанції LUFFT Ventus-UMB (' + config.meteo.host + ':' + config.meteo.port + '). Дані востаннє були отримані ' + new Date(weather.lastDT).toLocaleTimeString() + '</b>' // html body
    //        };
    //
    //        if ((now - config.mail.mailLast) > config.mail.mailAfter){
    //            // ввостаннє відправляв почту годину тому (config.mail.mailAfter), можно знову відправляти!
    //            transporter.sendMail(mailOptions, (error, info) => {
    //                if (error) {
    //                    return logger.error(error);
    //                }
    //
    //                logger.info('Message %s sent: %s', info.messageId, info.response);
    //                config.mail.mailLast = now;
    //            })
    //        }
    //
    //    }
    //}
