<?php

/*
    DB_HOST
    DB_PORT
    DB_NAME
    DB_USER
    DB_PASS
    DB_ROLE
    DB_ENC

    POINT_ID

    WIND_SPEED_AVG
    WIND_SPEED
    WIND_DIR
    TEMP
    PRESSURE
    HUMIDITY
    DEV_POINT
*/

    //var_dump($argv);

    foreach ($argv as $arg){
        $e = explode("=", $arg);
        if(count($e)==2){
            switch ($e[0]) {
                case 'DB_HOST':
                    $DB_HOST = $e[1];
                    break;
                case 'DB_PORT':
                    $DB_PORT = $e[1];
                    break;
                case 'DB_NAME':
                    $DB_NAME = $DB_HOST.':'.$e[1];
                    break;
                case 'DB_USER':
                    $DB_USER = $e[1];
                    break;
                case 'DB_PASS':
                    $DB_PASS = $e[1];
                    break;
                case 'DB_ROLE':
                    $DB_ROLE = $e[1];
                    break;
                case 'DB_ENC':
                    $DB_ENC = $e[1];
                    break;

                case 'POINT_ID':
                    $POINT_ID = $e[1];
                    break;

                case 'WIND_SPEED_AVG':
                    $WIND_SPEED_AVG = $e[1];
                    break;
                case 'WIND_SPEED':
                    $WIND_SPEED = $e[1];
                    break;
                case 'WIND_DIR':
                    $WIND_DIR = $e[1];
                    break;
                case 'TEMP':
                    $TEMP = $e[1];
                    break;
                case 'PRESSURE':
                    $PRESSURE = $e[1];
                    break;
                case 'HUMIDITY':
                    $HUMIDITY = $e[1];
                    break;
                case 'DEV_POINT':
                    $DEV_POINT = $e[1];
                    break;
            }
        }
    }

    $data['wind_direction'] = $WIND_DIR;
    $data['wind_speed_avg'] = $WIND_SPEED_AVG;
    $data['wind_speed'] = $WIND_SPEED;
    $data['temperature'] = $TEMP;
    $data['pressure'] = $PRESSURE;
    $data['humidity'] = $HUMIDITY;
    $data['dew_point'] = $DEV_POINT;

    IB_update_db($data);

//------------------------------------------------------------------------------
function IB_update_db(&$data){

    $fberror = false; 
    $testfb = false;
    // соединяемся с базой	   
    // сначала соединяемся сокетом и проверяем есть ли сервак в сетке, т.к. в йобаном интербейсе нет таймаута соединения
    $testfb = fsockopen($GLOBALS['DB_HOST'], $GLOBALS['DB_PORT'], $errno, $errstr, 3);
    if(!$testfb)
        $fberror = true;   	  
    else
        if (is_resource($testfb)) fclose($testfb);
  
    if($GLOBALS['DB_NAME'] != '' && !$fberror){
         
        // соединяемся с базой
        //echo $GLOBALS['DB_NAME'];
        if(!($connect = ibase_connect($GLOBALS['DB_NAME'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASS'], $GLOBALS['DB_ENC'], 1000, 3, $GLOBALS['DB_ROLE']))){
            //sendMail('Не могу соединиться с базой Interbase. '.ibase_errmsg());
            return false;
        }
        else{
            $query = "select visibility from VISIBILITIES where POINT_ID=".$GLOBALS['POINT_ID'].";";

            $result = ibase_query($connect,$query) or logger(ibase_errmsg()." ::".$query);
            $result = ibase_fetch_row($result);
            $GLOBALS['visibility']  = $result[0];

            // Повертаю видимість в stdout (visibility=80)
            echo 'visibility='.$GLOBALS['visibility'];

            $query = 'update weather 
                      set wind_speed_avg='.$data['wind_speed_avg'].', wind_speed='.$data['wind_speed'].',
                      wind_direction='.$data['wind_direction'].', temperature_outside='.$data['temperature'].',
                      pressure='.$data['pressure'].', humidity='.$data['humidity'].', dew_point='.$data['dew_point'].', 
                      update_date=\'NOW\' where point_id='.$GLOBALS['POINT_ID'].';';

            //if($GLOBALS['debug']) logger($query);
            
            $result = ibase_query($connect,$query) or logger(ibase_errmsg()." ::".$query);

            @ibase_close($connect);   
        }
    }    

}
//------------------------------------------------------------------------------

?>