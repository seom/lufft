/**
 * Created by Andriy Kulyk on 10.05.17.
 */

// { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }
const winston = require('winston');
const fs = require('fs');
const env = process.env.NODE_ENV || 'development';
const logDir = 'log';

// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const tsFormat = () => (new Date()).toLocaleTimeString();

var logger = module.exports = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            timestamp: tsFormat,
            colorize: true,
            level: env === 'development' ? 'debug' : 'info'
        }),
        new (require('winston-daily-rotate-file'))({
            filename: `${logDir}/-output.log`,
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            zippedArchive: true,
            maxFiles: 10,
            json: false,
            level: env === 'development' ? 'debug' : 'info'
        })

    ]
});