#! /bin/sh
npm install forever -g
cp dts_meteo2db_lufft /etc/init.d/dts_meteo2db_lufft
chmod 755 /etc/init.d/dts_meteo2db_lufft
cd /etc/init.d && update-rc.d dts_meteo2db_lufft defaults

#groupadd meteo