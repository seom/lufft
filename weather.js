/**
 * Created by Andriy Kulyk on 20.04.17.
 */
'use strict';

module.exports = function(socket){

    return new Weather(socket);
};

const logger =require('./logger');
const pgp = require('pg-promise')();
// You can check for all default values in:
// https://github.com/brianc/node-postgres/blob/master/lib/defaults.js
var cn = config.pg;
var db = pgp(cn); // database instance;

// ---------------------------------------------------------------------------------------------------------------------
// Weather constructor
// ---------------------------------------------------------------------------------------------------------------------
function Weather(socket) {
    this.arrWindSpeed = [];
    this.arrWindDir = [];
    this.windSpeedMax = 0;

    //wind 400 - cur, 440 - max, 460 - avg
    this.weather_params = {
        temp: '& 32769 M 00100\r\n',
        pressure: '& 32769 M 00300\r\n',
        wind: '& 32769 M 00400\r\n',
        windDir: '& 32769 M 00500\r\n'
    };

    this.socket = socket;

    this.temp = 0;
    this.pressure = 0;
    this.windSpeed = 0;
    this.windDir = 0;
    this.visibility = -1;

    this.newData = false;
    this.lastDT = undefined;
}

// --------------------------------------------------

Weather.prototype.printCurWeather = function() {
    logger.info(`Temp: ${this.temp} C\u00b0 | P: ${this.pressure} мм рт. ст. | Wind: ${this.windSpeed} m/s | WindDir: ${this.windDir}\u00b0`);
};

// --------------------------------------------------

Weather.prototype.getTemp = function() {
    return this.socket.write(this.weather_params.temp)
};

Weather.prototype.getPressure = function() {
    return this.socket.write(this.weather_params.pressure);
};

Weather.prototype.getWindDir = function () {
    return this.socket.write(this.weather_params.windDir);
};

Weather.prototype.getWind = function () {
    return this.socket.write(this.weather_params.wind);
};

// --------------------------------------------------

Weather.prototype.setTemp = function(data) {
    this.newData = true;
    var val = +data.toString().substr(16, 6); // преобразование к числу
    this.temp = (70 -(-50))/ 65520 * val + (-50);
    this.temp = Math.round(this.temp * 10) / 10;

    this.lastDT = Date.now();//new Date().toLocaleTimeString();
    config.mail.mailLast = 0;
};

Weather.prototype.setPressure = function(data) {
    this.newData = true;
    var val = +data.toString().substr(16, 6);
    this.pressure = 900 / 65520 * val + 300; //гПа
    this.pressure = this.pressure * 0.75; //1 гПа = 0.750061683 миллиметра ртутного столба
    this.pressure = Math.round(this.pressure * 10) / 10;
};

Weather.prototype.setWind = function(data) {
    this.newData = true;
    var val = +data.toString().substr(16, 6);
    this.windSpeed = 75 / 65520 * val; //m/s 0-75
    this.windSpeed = Math.round(this.windSpeed * 10) / 10;

    this.arrWindSpeed.push(this.windSpeed);
    this.windSpeedMax = this.windSpeed > this.windSpeedMax ? this.windSpeed : this.windSpeedMax;
};

Weather.prototype.setWindDir = function(data) {
    this.newData = true;
    var val = +data.toString().substr(16, 6);
    this.windDir = 359.9 / 65520 * val; //m/s
    this.windDir = Math.round(this.windDir * 10) / 10;

    this.arrWindDir.push(this.windDir);
};

// --------------------------------------------------

Weather.prototype.timer_1_sec = function() {
    if (new Date().getSeconds() % 10 == 0){
        logger.debug('10 SECONDS !!!');
        this.updatePG();

        //let now = Date.now();
        logger.debug(Date.now() - this.lastDT);
        if ( ((Date.now() - this.lastDT) / 1000) > 10 ){
            logger.warn('Дані відсудні протягом 10 секунд!');
            weather.getTemp();
        }


        if (new Date().getSeconds() % 60 == 0){
            logger.debug('MINUTE !!!');
            this.updateFB();
        }

        this.newData = false;

        this.arrWindDir.length = this.arrWindSpeed.length = 0;
        this.windSpeedMax = 0;
    }
};

// --------------------------------------------------

Weather.prototype.getMeteoData = function() {
    for (var i = 0, sum = 0; i < this.arrWindSpeed.length; i++) {
        sum += this.arrWindSpeed[i];
    }

    return {
        windSpeedAvg: ( this.arrWindSpeed.length > 0 ) ? ( Math.round(((sum / this.arrWindSpeed.length) * 100)) / 100 ) : 0,
        windSpeedMax: this.windSpeedMax || 0,
        windDirection: this.windDir || 0,
        pressure: this.pressure || 0,
        temperature: this.temp || 0,
        devPoint: 0,
        visibility: this.visibility || 0,
        humidity: 0
    };
};

// --------------------------------------------------
// Оновлюю дані у базі сайту
// --------------------------------------------------
Weather.prototype.updatePG = function( data = this.getMeteoData() ) {
    if (!this.newData) {
        logger.warn('updatePG: No new data.');
        return;
    }

    var s = '';

    logger.debug('>>> arrWindSpeed : [' + this.arrWindSpeed.toString() + '] arrWindDir : [' + this.arrWindDir.toString() + ']');
    logger.debug('>>> windSpeedAvg : ' + data.windSpeedAvg + ' windSpeedMax : ' + data.windSpeedMax + ' windDir : ' + data.windDirection);
    logger.debug('>>> pressure : ' + data.pressure);
    logger.debug('>>> temperature : ' + data.temperature);

    if(data.visibility == -1){
        // Якщо ще не отримав дані видимості з БД Дельта-Навігатор, то не оновлюю в БД сайта
        s = 'UPDATE vts_weather ' +
            'SET update_date=\'NOW\', wind_speed=$1, wind_speed_avg=$2, wind_direction=$3, temperature_outside=$4, pressure=$5 ' +
            'WHERE point_id=$6';

        db.none(s, [data.windSpeedMax, data.windSpeedAvg, data.windDirection, data.temperature, data.pressure, config.pg.point_id])
            .then(() => {
                logger.debug('updatePG success.');
            })
            .catch(error => {
                logger.error('updatePG error! ' + error);
            });
    }else{
        s = 'UPDATE vts_weather ' +
            'SET update_date=\'NOW\', wind_speed=$1, wind_speed_avg=$2, wind_direction=$3, temperature_outside=$4, pressure=$5, visibility=$6 ' +
            'WHERE point_id=$7';

        db.none(s, [data.windSpeedMax, data.windSpeedAvg, data.windDirection, data.temperature, data.pressure, data.visibility, config.pg.point_id])
            .then(() => {
                logger.debug('updatePG success.');
            })
            .catch(error => {
                logger.error('updatePG error! ' + error);
            });
    }
};

// --------------------------------------------------
// Оновлюю дані у базі Дельта-Навігатора
// --------------------------------------------------
Weather.prototype.updateFB = function( data = this.getMeteoData() ) {
    if (!this.newData) {
        logger.warn('updateFB: No new data.');
        return;
    }

    // Готую параметри для php скрипта, та виконую його
    const exec = require('child_process').exec;
    var fb = config.fb;

    const util = require('util');
    var s = util.format('php firebird.php DB_HOST=%s DB_PORT=%d DB_NAME=%s DB_USER=%s DB_PASS=%s DB_ROLE=%s DB_ENC=%s ' +
                        'POINT_ID=%d WIND_SPEED_AVG=%d WIND_SPEED=%d WIND_DIR=%d TEMP=%d PRESSURE=%d HUMIDITY=%d DEV_POINT=%d',
                        fb.host, fb.port, fb.database, fb.user, fb.password, fb.role, fb.encoding, fb.point_id,
                        data.windSpeedAvg, data.windSpeedMax, data.windDirection, data.temperature, data.pressure,
                        data.humidity, data.devPoint);

    logger.debug('Exec (firebird.php) script > %s', s);

    exec(s, (error, stdout, stderr) => {
        if (error) {
            logger.error(`exec error: ${error}`);
            return;
        }
        logger.debug('Exec (firebird.php) success.');
        logger.debug(`stdout: ${stdout}`);
        // Виконання php-скрипта повинно вернути видимість накшталт visibility=80.
        // Видиміть повинна бути записана у БД сайту
        var arr = stdout.split('=', 2);
        if(arr && (arr[0] == 'visibility')) this.visibility = +arr[1];
        logger.debug(`stderr: ${stderr}`);
    });

};
// ---------------------------------------------------------------------------------------------------------------------
// Weather constructor
// ---------------------------------------------------------------------------------------------------------------------