/**
 * Created by Andriy Kulyk on 04.05.17.
 */

config = {
    station: 'LUFFT Ventus-UMB',
    post: 'RK',
    host: '192.168.102.61',
    createHTTPServer: true,
    meteo: {
        host: '192.168.102.185',
        port: 4001,
        requestDelay: 500, // Затримка між запитами до метеостанції
        reconnectTimeout: 1000
    },
    pg: {
        host: 'localhost', // 'localhost' is the default;
        port: 5432,
        database: 'vts_bdlk_db',
        user: 'dts',
        password: 'nhfyckzwbzlfyys[',
        point_id: 1
    },
    fb: {
        host: '192.168.102.98',
        port: 3050,
        database: 'c:\\\\Delta-Navigator\\\\Shared\\\\Databases\\\\BDLK_PLANSHET.GDB',
        user: 'WEBSERVER',
        password: 'rk_web',
        role: 'WEBSERVER_ROLE',
        encoding: 'WIN1251',
        point_id: 1
    },
    httpServer: {
        httpUse: true, // Чи відсилати взагалі почту
        httpPort: 4002
    },
    mail: {
        mailUse: true, // Чи відсилати взагалі почту
        mailTimeout: 10*60*1000, // Не було даних протягом 10 хв.
        mailAfter: 60*60*1000, // Знову відправити почту через 1 год.
        mailTo: 'seom.aa@gmail.com', // comma separated list of receivers
        mailLast: 0 // Востаннє почта відправлялась (для службових цілей)
    }
};

module.exports = config;